import 'dart:io';

import 'package:flutter/material.dart';

class Env {
  static EnvData _env;

  static EnvData get data => _env;

  static void set(EnvData env) {
    _env = env;
  }

  static final EnvData dev = EnvData(
    apiBaseUrl: 'https://api.backendless.com/F23D385C-ECC0-7CB3-FF53-1628339F6D00/5E898762-A536-4080-AE05-B683E3BDA101',
    cloudMessagingKey:
        'AAAAOCU7Gb4:APA91bH1aSySU_tFVdhIvX92Y8yDEi3aSUry6U17kyxLEvCXYDOMfA2aqkn9m1IPzKd59U-2W3EtSeLxlBGgcywnxXlUoqJN-07KH16uYTk0uzUVhZU4EKNln_nJdYGbU8sIrzhcSoak',
  );

  static final EnvData prod = EnvData(
    apiBaseUrl: 'https://api.backendless.com/F23D385C-ECC0-7CB3-FF53-1628339F6D00/5E898762-A536-4080-AE05-B683E3BDA101',
    cloudMessagingKey:
        'AAAAOCU7Gb4:APA91bH1aSySU_tFVdhIvX92Y8yDEi3aSUry6U17kyxLEvCXYDOMfA2aqkn9m1IPzKd59U-2W3EtSeLxlBGgcywnxXlUoqJN-07KH16uYTk0uzUVhZU4EKNln_nJdYGbU8sIrzhcSoak',
  );

  static String _getDevApi() {
    return Platform.isAndroid ? "http://10.0.2.2:5000" : "http://127.0.0.1:5000";
  }
}

class EnvData {
  final String apiBaseUrl;
  final String cloudMessagingKey;

  EnvData({
    @required this.apiBaseUrl,
    @required this.cloudMessagingKey,
  });
}
