import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class CommonMethods{
  static void showToast(String message) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 2,
      backgroundColor: Colors.grey.shade300,
      textColor: Colors.black,
    );
  }

  ///More references - https://www.journaldev.com/17899/java-simpledateformat-java-date-format
  static String getFormatDateTime(DateTime date,
      {String format = 'dd.MM.yy, HH:mm', bool includeTodayYesterdayChecks = true}) {
    DateTime now = new DateTime.now();
    DateTime today = DateTime(now.year, now.month, now.day);
    DateTime yesterday = DateTime(now.year, now.month, now.day - 1);

    DateTime localTime = date.toLocal();
    DateTime givenDateTimeForComparing = DateTime(localTime.year, localTime.month, localTime.day);

    String formattedDate = "";
    if (includeTodayYesterdayChecks) {
      if (givenDateTimeForComparing == today) {
        DateFormat df = DateFormat("hh:mm a");
        formattedDate = df.format(localTime);
      } else if (givenDateTimeForComparing == yesterday) {
        formattedDate = "Yesterday";
      } else {
        DateFormat df = DateFormat(format);
        formattedDate = df.format(localTime);
      }
    } else {
      DateFormat df = DateFormat(format);
      formattedDate = df.format(localTime);
    }

    return formattedDate;
  }
}