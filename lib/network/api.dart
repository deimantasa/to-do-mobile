import 'dart:io';

import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/util/env.dart';

class Api {
//  ------------------------------------------
//  HEADERS
//  ------------------------------------------
  static Map<String, String> kHeaderContentJson = {HttpHeaders.contentTypeHeader: "application/json"};

//  ------------------------------------------
//  PUSH NOTIFICATIONS API
//  ------------------------------------------
  static String pushNotificationUrl() {
    return "https://fcm.googleapis.com/fcm/send";
  }

//  ------------------------------------------
//  Tasks API
//  ------------------------------------------

  static String getMyTasks(String username) {
    return Env.data.apiBaseUrl +
        "/data/tasks?where=isArchived=false AND user.name='$username'&pageSize=100&sortBy=created asc";
  }

  static String getAllTasks() {
    return Env.data.apiBaseUrl + "/data/tasks?where=isArchived=false&pageSize=100&sortBy=created asc";
  }

  static String getMyArchivedTasks(String username) {
    return Env.data.apiBaseUrl +
        "/data/tasks?where=isArchived=true AND user.name='$username'&pageSize=100&sortBy=updated desc";
  }

  static String getUserByName(String name) {
    return Env.data.apiBaseUrl + "/data/Users?where=name='$name'";
  }

  static String addTask() {
    return Env.data.apiBaseUrl + "/data/tasks";
  }

  static addUserToTask(String taskId) {
    return Env.data.apiBaseUrl + "/data/tasks/$taskId/user";
  }

  static updateTask(String taskId) {
    return Env.data.apiBaseUrl + "/data/tasks/$taskId";
  }
}
