import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:to_do_list/managers/push_notification_manager.dart';
import 'package:to_do_list/models/user.dart';

class UserStore extends ChangeNotifier {
  static const String _kUserKey = "userKey";

  User user;

  Future<void> setUser(User user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //null will be only if logging out
    if (user == null) {
      prefs.setString(_kUserKey, null);
    } else {
      prefs.setString(_kUserKey, json.encode(user.toJson()));
    }
    this.user = user;
  }

  //Only used to retrieve user first time
  Future<void> initUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userStringJson = prefs.getString(_kUserKey);
    if (userStringJson != null) {
      User user = User.fromJson(json.decode(userStringJson));
      this.user = user;
    }
  }

  void logout() {
    setUser(null);
    GetIt.instance<PushNotificationManager>().unsubscribeFromTopic(PushNotificationManager.kTopicAll);
  }
}
