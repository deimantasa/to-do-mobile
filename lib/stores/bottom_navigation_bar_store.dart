import 'package:flutter/cupertino.dart';
import 'package:to_do_list/pages/all_tasks/all_tasks_page.dart';
import 'package:to_do_list/pages/archived_tasks/archived_tasks_page.dart';
import 'package:to_do_list/pages/my_tasks/my_tasks_page.dart';
import 'package:to_do_list/pages/profile/profile_page.dart';

class BottomNavigationBarStore extends ChangeNotifier {
  static const int kMyTasksPageIndex = 0;
  static const int kAllTasksPageIndex = 1;
  static const int kArchivedTasksPageIndex = 2;
  static const int kProfilePageIndex = 3;

  int bottomNavigationBarSelectedIndex = 0;

  Widget getCurrentTab() {
    switch (bottomNavigationBarSelectedIndex) {
      //Don't user names, because it might get confusing later
      //emphasise on tab numbers instead
      case 0:
        return MyTasksPage();
      case 1:
        return AllTasksPage();
      case 2:
        return ArchivedTasksPage();
      case 3:
        return ProfilePage();
      //Should not happen
      default:
        throw Exception("Bottom Navigation Bar Tab is not defined");
    }
  }
}
