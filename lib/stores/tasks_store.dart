import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:to_do_list/managers/push_notification_manager.dart';
import 'package:to_do_list/models/loading_state.dart';
import 'package:to_do_list/models/task.dart';
import 'package:http/http.dart' as http;
import 'package:to_do_list/network/api.dart';
import 'package:to_do_list/stores/user_store.dart';

class TasksStore extends ChangeNotifier {
  LoadingState myTasksLoadingState = LoadingState.notLoading();
  LoadingState allTasksLoadingState = LoadingState.notLoading();
  LoadingState archivedTasksLoadingState = LoadingState.notLoading();

  List<Task> myTasks = [];
  List<Task> allTasks = [];
  List<Task> archivedTasks = [];

  void initTasks(UserStore userStore) {
    myTasksLoadingState.isLoading = true;
    getMyTasks(userStore).then((value) {
      myTasks = value;
      myTasksLoadingState.isLoading = false;
      notifyListeners();
    });

    getAllTasks().then((value) => allTasks = value);
    getMyArchivedTasks(userStore).then((value) => archivedTasks = value);
  }

  Future<List<Task>> getMyTasks(UserStore userStore) async {
    final http.Response response = await http.get(Api.getMyTasks(userStore.user.name), headers: Api.kHeaderContentJson);
    List<Task> myTasks = [];
    if (response.statusCode == 200) {
      List<dynamic> tasksJson = json.decode(response.body);
      if (tasksJson != null) {
        for (var taskJson in tasksJson) {
          Task task = Task.fromJson(taskJson);
          myTasks.add(task);
        }
      }
    } else {
      myTasks = null;
    }
    return myTasks;
  }

  Future<List<Task>> getAllTasks() async {
    final http.Response response = await http.get(Api.getAllTasks(), headers: Api.kHeaderContentJson);
    List<Task> allTasks = [];
    if (response.statusCode == 200) {
      List<dynamic> tasksJson = json.decode(response.body);
      if (tasksJson != null) {
        for (var taskJson in tasksJson) {
          Task task = Task.fromJson(taskJson);
          allTasks.add(task);
        }
      }
    } else {
      allTasks = null;
    }
    return allTasks;
  }

  Future<List<Task>> getMyArchivedTasks(UserStore userStore) async {
    final http.Response response = await http.get(Api.getMyArchivedTasks(userStore.user.name), headers: Api.kHeaderContentJson);
    List<Task> achievedTasks = [];
    if (response.statusCode == 200) {
      List<dynamic> tasksJson = json.decode(response.body);
      if (tasksJson != null) {
        for (var taskJson in tasksJson) {
          Task task = Task.fromJson(taskJson);
          achievedTasks.add(task);
        }
      }
    } else {
      achievedTasks = null;
    }
    return achievedTasks;
  }

  void addNewMyTask(Task task) {
    myTasks.add(task);
    allTasks.add(task);
    notifyListeners();
  }

  Future<void> moveTaskToArchive(Task task) async {
    final http.Response response =
        await http.put(Api.updateTask(task.objectId), body: json.encode({'isArchived': true}), headers: Api.kHeaderContentJson);
    if (response.statusCode == 200) {
      myTasks.removeWhere((element) => element.objectId == task.objectId);
      //Remove task from all tasks, because all tasks doesn't show items which are checked
      allTasks.removeWhere((element) => element.objectId == task.objectId);
      //Everything went well, therefore update task offline and add to archive
      task.isSelected = false;
      task.isArchived = true;
      //Since we are showing time when task was checked off
      //add current time as we don't want to re-fetch whole item from db
      task.updated = DateTime.now().millisecondsSinceEpoch;
      archivedTasks.insert(0, task);
      notifyListeners();
    } else {
      //TODO currently we launch it silently therefore failing silently too
    }
  }

  Future<void> moveTaskToMyTasks(Task task) async {
    final http.Response response =
        await http.put(Api.updateTask(task.objectId), body: json.encode({'isArchived': false}), headers: Api.kHeaderContentJson);
    if (response.statusCode == 200) {
      archivedTasks.removeWhere((element) => element.objectId == task.objectId);
      //Everything went well, therefore update task offline and add to archive
      task.isSelected = false;
      task.isArchived = false;
      myTasks.add(task);
      //When item is moved to my tasks, it means that item is not checked anymore
      // therefore it should be visible in All Tasks list
      allTasks.add(task);
      notifyListeners();
    } else {
      //TODO currently we launch it silently therefore failing silently too
    }
  }

  void updateAllTasks(List<Task> allTasks) {
    this.allTasks = allTasks;
    notifyListeners();
  }

  void reset() {
    myTasks.clear();
    allTasks.clear();
    archivedTasks.clear();
  }
}
