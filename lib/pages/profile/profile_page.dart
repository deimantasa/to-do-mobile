import 'package:flutter/material.dart';
import 'package:to_do_list/pages/sign_in/sign_in_page.dart';
import 'package:to_do_list/util/common_methods.dart';

import 'profile_contract.dart';
import 'profile_model.dart';
import 'profile_presenter.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> implements View {
  ProfileModel _profileModel;
  ProfilePresenter _profilePresenter;

  @override
  void initState() {
    _profileModel = ProfileModel();
    _profilePresenter = ProfilePresenter(this, context, _profileModel);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ProfilePage"),
      ),
      body: _getBodyWidget(),
    );
  }

  @override
  void showMessage(String message) {
    CommonMethods.showToast(message);
  }

  @override
  void updateView() {
    if (mounted) setState(() {});
  }

  Widget _getBodyWidget() {
    return Center(
      child: RaisedButton(
        child: Text("Logout"),
        onPressed: () => _showLogoutConfirmationDialog(),
      ),
    );
  }

  void _showLogoutConfirmationDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Log-out"),
            content: Text("Are you sure want to logout?"),
            actions: [
              FlatButton(
                child: Text("No"),
                onPressed: () => Navigator.pop(context),
              ),
              FlatButton(
                child: Text("Yes"),
                onPressed: () => _profilePresenter.logout(),
              ),
            ],
          );
        });
  }

  @override
  void goToLoginPage() {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) => SignInPage(),
        ),
        (Route<dynamic> route) => false);
  }
}
