abstract class Presenter{
  void logout();
}

abstract class View{
	void updateView();
	void showMessage(String message);
  void goToLoginPage();
}