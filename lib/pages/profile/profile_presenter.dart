import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/stores/tasks_store.dart';
import 'package:to_do_list/stores/user_store.dart';

import 'profile_contract.dart';
import 'profile_model.dart';

class ProfilePresenter implements Presenter {
  View _view;
  ProfileModel _profileModel;

  UserStore _userStore;
  TasksStore _tasksStore;

  ProfilePresenter(View view, BuildContext context, ProfileModel profileModel) {
    _view = view;
    _profileModel = profileModel;

    _userStore = context.read<UserStore>();
    _tasksStore = context.read<TasksStore>();
  }

  @override
  void logout() {
    _userStore.logout();
    _tasksStore.reset();
    _view.goToLoginPage();
  }
}
