import 'package:to_do_list/models/loading_state.dart';
import 'package:to_do_list/models/task.dart';

class ArchivedTasksModel {
  LoadingState fetchingArchivedTasksLoadingState = LoadingState.notLoading();
  List<Task> archivedTasks = [];
}
