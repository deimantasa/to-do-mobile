abstract class Presenter{
	void refreshTasks();
	void moveSelectedTasksToMyTasks();
}

abstract class View{
	void updateView();
	void showMessage(String message);
}