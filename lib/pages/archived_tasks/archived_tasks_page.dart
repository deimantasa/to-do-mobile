import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/stores/tasks_store.dart';
import 'package:to_do_list/util/common_methods.dart';
import 'package:to_do_list/widgets/loading_widget.dart';

import 'archived_tasks_contract.dart';
import 'archived_tasks_model.dart';
import 'archived_tasks_presenter.dart';

class ArchivedTasksPage extends StatefulWidget {
  @override
  _ArchivedTasksPageState createState() => _ArchivedTasksPageState();
}

class _ArchivedTasksPageState extends State<ArchivedTasksPage> implements View {
  ArchivedTasksModel _archivedTasksModel;
  ArchivedTasksPresenter _archivedTasksPresenter;

  @override
  void initState() {
    _archivedTasksModel = ArchivedTasksModel();
    _archivedTasksPresenter = ArchivedTasksPresenter(this, context, _archivedTasksModel);
    super.initState();
  }

  @override
  void dispose() {
    _archivedTasksPresenter.moveSelectedTasksToMyTasks();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    context.watch<TasksStore>();
    _archivedTasksPresenter.refreshTasks();

    return Scaffold(
      appBar: AppBar(
        title: Text("Archived Tasks Page"),
      ),
      body: _getBodyWidget(),
    );
  }

  @override
  void showMessage(String message) {
    CommonMethods.showToast(message);
  }

  @override
  void updateView() {
    if (mounted) setState(() {});
  }

  Widget _getBodyWidget() {
    if (_archivedTasksModel.fetchingArchivedTasksLoadingState.isLoading) {
      return Center(
        child: LoadingWidget(),
      );
    } else {
      if (_archivedTasksModel.archivedTasks.isEmpty) {
        return Center(
          child: Text("There are no archived tasks"),
        );
      } else {
        return ListView.builder(
            itemCount: _archivedTasksModel.archivedTasks.length,
            itemBuilder: (context, itemIndex) {
              Task task = _archivedTasksModel.archivedTasks[itemIndex];
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    leading: Checkbox(
                      value: task.isSelected,
                      onChanged: (bool isSelected) {
                        setState(() {
                          task.isSelected = isSelected;
                        });
                      },
                    ),
                    title: Text("${task.content}"),
                    subtitle: Text("Completed: ${CommonMethods.getFormatDateTime(DateTime.fromMillisecondsSinceEpoch(task.updated))}"),
                  ),
                  Divider(
                    height: 1,
                  ),
                ],
              );
            });
      }
    }
  }
}
