import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/stores/tasks_store.dart';

import 'archived_tasks_contract.dart';
import 'archived_tasks_model.dart';

class ArchivedTasksPresenter implements Presenter {
  View _view;
  ArchivedTasksModel _archivedTasksModel;

  TasksStore _tasksStore;

  ArchivedTasksPresenter(View view, BuildContext context, ArchivedTasksModel archivedTasksModel) {
    _view = view;
    _archivedTasksModel = archivedTasksModel;

    _tasksStore = context.read<TasksStore>();
  }

  @override
  void refreshTasks() {
    _archivedTasksModel.fetchingArchivedTasksLoadingState = _tasksStore.archivedTasksLoadingState;
    _archivedTasksModel.archivedTasks = _tasksStore.archivedTasks;
    _view.updateView();
  }

  @override
  void moveSelectedTasksToMyTasks() {
    for (Task task in _archivedTasksModel.archivedTasks) {
      if (task.isSelected) {
        _tasksStore.moveTaskToMyTasks(task);
      }
    }
  }
}
