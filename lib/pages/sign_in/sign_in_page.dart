import 'package:flutter/material.dart';
import 'package:to_do_list/pages/home/home_page.dart';
import 'package:to_do_list/util/common_methods.dart';
import 'package:to_do_list/widgets/loading_widget.dart';

import 'sign_in_contract.dart';
import 'sign_in_model.dart';
import 'sign_in_presenter.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> implements View {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _textEditingController = TextEditingController();

  SignInModel _signInModel;
  SignInPresenter _signInPresenter;

  @override
  void initState() {
    _signInModel = SignInModel();
    _signInPresenter = SignInPresenter(this, context, _signInModel);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SignInPage"),
      ),
      body: _getBodyWidget(),
    );
  }

  @override
  void showMessage(String message) {
    CommonMethods.showToast(message);
  }

  @override
  void updateView() {
    if (mounted) setState(() {});
  }

  Widget _getBodyWidget() {
    if (_signInModel.signInLoadingState.isLoading) {
      return Center(child: LoadingWidget());
    } else {
      return Center(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  controller: _textEditingController,
                  decoration: InputDecoration(hintText: "Enter your name", labelText: "Name"),
                  onChanged: (input) => _signInModel.name = input,
                  validator: (input) => input.isNotEmpty ? null : "Name cannot be empty",
                ),
                SizedBox(
                  height: 8,
                ),
                RaisedButton(
                  child: Text("Continue"),
                  onPressed: () => _validateName(),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  void _validateName() {
    if (_formKey.currentState.validate()) {
      _signInPresenter.signIn();
    }
  }

  @override
  void goToHomePage() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
  }
}
