abstract class Presenter{
	Future<void> signIn();
}

abstract class View{
	void updateView();
	void showMessage(String message);
	void goToHomePage();
}