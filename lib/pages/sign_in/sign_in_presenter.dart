import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:to_do_list/managers/push_notification_manager.dart';
import 'package:to_do_list/models/user.dart';
import 'package:to_do_list/network/api.dart';
import 'package:to_do_list/stores/user_store.dart';
import 'package:to_do_list/util/env.dart';

import 'sign_in_contract.dart';
import 'sign_in_model.dart';

class SignInPresenter implements Presenter {
  View _view;
  SignInModel _signInModel;

  UserStore _userStore;

  SignInPresenter(View view, BuildContext context, SignInModel signInModel) {
    _view = view;
    _signInModel = signInModel;

    _userStore = context.read<UserStore>();
  }

  @override
  Future<void> signIn() async {
    _signInModel.signInLoadingState.isLoading = true;
    _view.updateView();

    final http.Response response = await http.get(Api.getUserByName(_signInModel.name), headers: Api.kHeaderContentJson);
    if (response.statusCode == 200) {
      //Backendless returns list of users by matching name. We should expect one item in the list only
      List<dynamic> usersJson = json.decode(response.body);
      if (usersJson != null && usersJson.isNotEmpty) {
        User user = User.fromJson(usersJson.first);
        _userStore.setUser(user);
        GetIt.instance<PushNotificationManager>().subscribeToTopic(PushNotificationManager.kTopicAll);
        _view.goToHomePage();
      } else {
        _view.showMessage("User not found");
      }
    }
    //Generic error with status not 200. Ideally we want to get exact error message to exactly point out to user what's wrong
    else {
      _view.showMessage("Cannot sign-in. Incorrect credentials");
    }

    _signInModel.signInLoadingState.isLoading = false;
    _view.updateView();
  }
}
