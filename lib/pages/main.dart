import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/managers/push_notification_manager.dart';
import 'package:to_do_list/pages/intro/intro_page.dart';
import 'package:to_do_list/stores/bottom_navigation_bar_store.dart';
import 'package:to_do_list/stores/tasks_store.dart';
import 'package:to_do_list/stores/user_store.dart';
import 'package:to_do_list/util/env.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  Env.set(kReleaseMode ? Env.prod : Env.dev);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    GetIt.instance.registerSingleton(PushNotificationManager());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<BottomNavigationBarStore>(create: (_) => BottomNavigationBarStore()),
        ChangeNotifierProvider<UserStore>(create: (_) => UserStore()),
        ChangeNotifierProvider<TasksStore>(create: (_) => TasksStore()),
      ],
      child: MaterialApp(
        title: 'To-Do List Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: IntroPage(),
      ),
    );
  }
}
