import 'package:flutter/material.dart';
import 'package:to_do_list/pages/sign_in/sign_in_page.dart';
import 'package:to_do_list/util/common_methods.dart';

import 'home_contract.dart';
import 'home_model.dart';
import 'home_presenter.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> implements View {
  HomeModel _homeModel;
  HomePresenter _homePresenter;

  @override
  void initState() {
    _homeModel = HomeModel();
    _homePresenter = HomePresenter(this, context, _homeModel);
    _homePresenter.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _getBodyWidget(),
    );
  }

  @override
  void showMessage(String message) {
    CommonMethods.showToast(message);
  }

  @override
  void updateView() {
    if (mounted) setState(() {});
  }

  Widget _getBodyWidget() {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        elevation: 5,
        currentIndex: _homePresenter.getCurrentIndex(),
        onTap: (index) => _homePresenter.goToTab(index),
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "My Tasks"),
          BottomNavigationBarItem(icon: Icon(Icons.folder), label: "All Tasks"),
          BottomNavigationBarItem(icon: Icon(Icons.archive), label: "Achieved Tasks"),
          BottomNavigationBarItem(icon: Icon(Icons.account_circle), label: "Profile"),
        ],
      ),
      body: _homePresenter.getCurrentTab(),
    );
  }
}
