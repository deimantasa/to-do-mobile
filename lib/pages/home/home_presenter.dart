import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/stores/bottom_navigation_bar_store.dart';
import 'package:to_do_list/stores/tasks_store.dart';
import 'package:to_do_list/stores/user_store.dart';

import 'home_contract.dart';
import 'home_model.dart';

class HomePresenter implements Presenter {
  View _view;
  HomeModel _homeModel;

  FirebaseMessaging _firebaseMessaging;

  BottomNavigationBarStore _bottomNavigationBarStore;
  UserStore _userStore;
  TasksStore _tasksStore;

  HomePresenter(View view, BuildContext context, HomeModel homeModel) {
    _view = view;
    _homeModel = homeModel;

    _firebaseMessaging = FirebaseMessaging();

    _bottomNavigationBarStore = context.read<BottomNavigationBarStore>();
    _userStore = context.read<UserStore>();
    _tasksStore = context.read<TasksStore>();
  }

  @override
  int getCurrentIndex() {
    return _bottomNavigationBarStore.bottomNavigationBarSelectedIndex;
  }

  @override
  Widget getCurrentTab() {
    return _bottomNavigationBarStore.getCurrentTab();
  }

  @override
  void goToTab(int index) {
    _bottomNavigationBarStore.bottomNavigationBarSelectedIndex = index;
    _view.updateView();
  }

  @override
  void init() {
    _tasksStore.initTasks(_userStore);
    _initPushNotifications();
  }

  void _initPushNotifications() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        //Retrieving used id who triggered push notification
        try {
          String userId = message['data']['userId'];
          if (userId != _userStore.user.objectId) {
            List<Task> allTasks = await _tasksStore.getAllTasks();
            _tasksStore.updateAllTasks(allTasks);
          }
        } catch (e) {
          print("${e.toString()}");
        }
        print("$message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        //TODO?
      },
      onResume: (Map<String, dynamic> message) async {
        //TODO
      },
    );

    _firebaseMessaging.requestNotificationPermissions(IosNotificationSettings(
      sound: true,
      badge: true,
      alert: true,
    ));
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
    });

    _firebaseMessaging.getToken().then((value) => print(value));
  }
}
