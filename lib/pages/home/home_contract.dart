import 'package:flutter/material.dart';

abstract class Presenter{
	int getCurrentIndex();
	void goToTab(int index);
	Widget getCurrentTab();

	void init();
}

abstract class View{
	void updateView();
	void showMessage(String message);
}