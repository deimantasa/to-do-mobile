abstract class Presenter{
  void refreshMyTasks();
  Future<void> addNewTask(String taskName);
  void moveSelectedTasksToArchive();
}

abstract class View{
	void updateView();
	void showMessage(String message);
}