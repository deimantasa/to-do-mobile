import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/models/loading_widget_size.dart';
import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/stores/tasks_store.dart';
import 'package:to_do_list/util/common_methods.dart';
import 'package:to_do_list/widgets/loading_widget.dart';

import 'my_tasks_contract.dart';
import 'my_tasks_model.dart';
import 'my_tasks_presenter.dart';

class MyTasksPage extends StatefulWidget {
  @override
  _MyTasksPageState createState() => _MyTasksPageState();
}

class _MyTasksPageState extends State<MyTasksPage> implements View {
  MyTasksModel _myTasksModel;
  MyTasksPresenter _myTasksPresenter;

  @override
  void initState() {
    _myTasksModel = MyTasksModel();
    _myTasksPresenter = MyTasksPresenter(this, context, _myTasksModel);
    super.initState();
  }


  @override
  void dispose() {
    _myTasksPresenter.moveSelectedTasksToArchive();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    context.watch<TasksStore>();
    _myTasksPresenter.refreshMyTasks();

    return Scaffold(
      appBar: AppBar(
        title: Text("MyTasksPage"),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        child: _myTasksModel.addingNewTaskLoadingState.isLoading
            ? LoadingWidget(
                loadingWidgetSize: LoadingWidgetSize.small(),
              )
            : Icon(Icons.add),
        onPressed: () => _myTasksModel.addingNewTaskLoadingState.isLoading ? null : _showAddNewTaskDialog(),
      ),
      body: _getBodyWidget(),
    );
  }

  @override
  void showMessage(String message) {
    CommonMethods.showToast(message);
  }

  @override
  void updateView() {
    if (mounted) setState(() {});
  }

  Widget _getBodyWidget() {
    if (_myTasksModel.fetchingMyTasksLoadingState.isLoading) {
      return Center(
        child: LoadingWidget(),
      );
    } else {
      if (_myTasksModel.myTasks.isEmpty) {
        return Center(
          child: Text("You've no tasks"),
        );
      } else {
        return ListView.builder(
            itemCount: _myTasksModel.myTasks.length,
            itemBuilder: (context, itemIndex) {
              Task task = _myTasksModel.myTasks[itemIndex];
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    leading: Checkbox(
                      value: task.isSelected,
                      onChanged: (bool isSelected) {
                        setState(() {
                          task.isSelected = isSelected;
                        });
                      },
                    ),
                    title: Text("${task.content}"),
                  ),
                  Divider(
                    height: 1,
                  ),
                ],
              );
            });
      }
    }
  }

  void _showAddNewTaskDialog() {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    final TextEditingController _contentEditingController = TextEditingController();
    showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setDialogState) {
              return AlertDialog(
                title: Text("Add new task"),
                content: Form(
                  key: _formKey,
                  child: TextFormField(
                    autofocus: true,
                    controller: _contentEditingController,
                    decoration: InputDecoration(hintText: "Clean the room", labelText: "Task"),
                    textCapitalization: TextCapitalization.sentences,
                    validator: (input) => input.isNotEmpty ? null : "Task cannot be empty",
                  ),
                ),
                actions: [
                  FlatButton(
                    child: Text("Cancel"),
                    onPressed: () => Navigator.pop(context),
                  ),
                  FlatButton(
                      child: Text("Add"),
                      onPressed: () {
                        setDialogState(() {
                          if (_formKey.currentState.validate()) {
                            Navigator.pop(context);
                            _myTasksPresenter.addNewTask(_contentEditingController.text);
                          }
                        });
                      }),
                ],
              );
            },
          );
        });
  }
}
