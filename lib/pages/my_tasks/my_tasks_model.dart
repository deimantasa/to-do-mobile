import 'package:to_do_list/models/loading_state.dart';
import 'package:to_do_list/models/task.dart';

class MyTasksModel {
  LoadingState fetchingMyTasksLoadingState = LoadingState.notLoading();
  LoadingState addingNewTaskLoadingState = LoadingState.notLoading();
  List<Task> myTasks = [];
}
