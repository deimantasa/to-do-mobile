import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/managers/push_notification_manager.dart';
import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/network/api.dart';
import 'package:to_do_list/stores/tasks_store.dart';
import 'package:http/http.dart' as http;
import 'package:to_do_list/stores/user_store.dart';

import 'my_tasks_contract.dart';
import 'my_tasks_model.dart';

class MyTasksPresenter implements Presenter {
  View _view;
  MyTasksModel _myTasksModel;

  TasksStore _tasksStore;
  UserStore _userStore;

  MyTasksPresenter(View view, BuildContext context, MyTasksModel myTasksModel) {
    _view = view;
    _myTasksModel = myTasksModel;

    _tasksStore = context.read<TasksStore>();
    _userStore = context.read<UserStore>();
  }

  @override
  void refreshMyTasks() {
    _myTasksModel.fetchingMyTasksLoadingState = _tasksStore.myTasksLoadingState;
    _myTasksModel.myTasks = _tasksStore.myTasks;
    _view.updateView();
  }

  @override
  Future<void> addNewTask(String taskName) async {
    _myTasksModel.addingNewTaskLoadingState.isLoading = true;
    _view.updateView();

    String userId = _userStore.user.objectId;
    Task task = Task.add(taskName, userId);
    final http.Response response =
        await http.post(Api.addTask(), body: json.encode(task.toJson()), headers: Api.kHeaderContentJson);
    if (response.statusCode == 200) {
      Task retrievedTask = Task.fromJson(json.decode(response.body));
      final http.Response putResponse =
          await http.put(Api.addUserToTask(retrievedTask.objectId), body: json.encode([userId]), headers: Api.kHeaderContentJson);
      if (putResponse.statusCode == 200) {
        //Assign objectId to existing item, don't override whole item because items from backendless by default
        //doesn't include related objects therefore in this case User would be lost
        task.objectId = retrievedTask.objectId;
        _tasksStore.addNewMyTask(task);

        //Send push notification to notify everyone about new task
        GetIt.instance<PushNotificationManager>().sendPushNotification(
          "New task added",
          task.content,
          PushNotificationManager.kTopicAll,
          userId,
        );
        _view.showMessage("New task added");
      }
      //Task was added but user didn't get linked to the task. Something's wrong
      else {
        _view.showMessage("Cannot add new task. Something went wrong (1)");
      }
    } else {
      _view.showMessage("Cannot add new task. Something went wrong (2)");
    }

    _myTasksModel.addingNewTaskLoadingState.isLoading = false;
    _view.updateView();
  }

  @override
  void moveSelectedTasksToArchive() {
    for (Task task in _myTasksModel.myTasks) {
      if (task.isSelected) {
        _tasksStore.moveTaskToArchive(task);
      }
    }
  }
}
