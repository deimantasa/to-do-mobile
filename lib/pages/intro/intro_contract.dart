abstract class Presenter{
	Future<void> getConfig();
}

abstract class View{
	void updateView();
	void showMessage(String message);
	void goToNextPage(bool isLoggedIn);
}