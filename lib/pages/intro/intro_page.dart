import 'package:flutter/material.dart';
import 'package:to_do_list/models/loading_widget_size.dart';
import 'package:to_do_list/pages/home/home_page.dart';
import 'package:to_do_list/pages/sign_in/sign_in_page.dart';
import 'package:to_do_list/util/common_methods.dart';
import 'package:to_do_list/widgets/loading_widget.dart';

import 'intro_contract.dart';
import 'intro_model.dart';
import 'intro_presenter.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> implements View {
  IntroModel _introModel;
  IntroPresenter _introPresenter;

  @override
  void initState() {
    _introModel = IntroModel();
    _introPresenter = IntroPresenter(this, context, _introModel);
    _introPresenter.getConfig();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBodyWidget(),
    );
  }

  @override
  void showMessage(String message) {
    CommonMethods.showToast(message);
  }

  @override
  void updateView() {
    if (mounted) setState(() {});
  }

  Widget _buildBodyWidget() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          FlutterLogo(size: 240,),
          SizedBox(height: 32),
          Center(
            child: LoadingWidget(
              loadingWidgetSize: LoadingWidgetSize.small(),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void goToNextPage(bool isLoggedIn) {
    if (isLoggedIn) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
    } else {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignInPage()));
    }
  }
}
