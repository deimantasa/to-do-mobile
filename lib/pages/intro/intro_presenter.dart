import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/models/user.dart';
import 'package:to_do_list/stores/user_store.dart';

import 'intro_contract.dart';
import 'intro_model.dart';

class IntroPresenter implements Presenter {
  View _view;
  IntroModel _introModel;

  UserStore _userStore;

  IntroPresenter(View view, BuildContext context, IntroModel introModel) {
    _view = view;
    _introModel = introModel;

    _userStore = context.read<UserStore>();
  }

  @override
  Future<void> getConfig() async {
    _introModel.fetchingConfigLoadingState.isLoading = true;
    _view.updateView();

    await Future.wait([
      Future.delayed(Duration(seconds: 2)),
      _userStore.initUser(),
    ]);

    //If user is null it means that user was not logged in therefore forward to signIn page
    //Otherwise forward to homePage
    _view.goToNextPage(_userStore.user != null);
  }
}
