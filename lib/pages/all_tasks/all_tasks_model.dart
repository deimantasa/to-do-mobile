import 'package:to_do_list/models/loading_state.dart';
import 'package:to_do_list/models/task.dart';

class AllTasksModel {
  LoadingState fetchingAllTasksLoadingState = LoadingState.notLoading();
  List<Task> allTasks = [];
}
