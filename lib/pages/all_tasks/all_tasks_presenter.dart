import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/stores/tasks_store.dart';

import 'all_tasks_contract.dart';
import 'all_tasks_model.dart';

class AllTasksPresenter implements Presenter {
  View _view;
  AllTasksModel _allTasksModel;

  TasksStore _tasksStore;

  AllTasksPresenter(View view, BuildContext context, AllTasksModel allTasksModel) {
    _view = view;
    _allTasksModel = allTasksModel;

    _tasksStore = context.read<TasksStore>();
  }

  @override
  void refreshTasks() {
    _allTasksModel.fetchingAllTasksLoadingState = _tasksStore.allTasksLoadingState;
    _allTasksModel.allTasks = _tasksStore.allTasks;
    _view.updateView();
  }
}
