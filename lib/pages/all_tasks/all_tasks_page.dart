import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/stores/tasks_store.dart';
import 'package:to_do_list/util/common_methods.dart';
import 'package:to_do_list/widgets/loading_widget.dart';

import 'all_tasks_contract.dart';
import 'all_tasks_model.dart';
import 'all_tasks_presenter.dart';

class AllTasksPage extends StatefulWidget {
  @override
  _AllTasksPageState createState() => _AllTasksPageState();
}

class _AllTasksPageState extends State<AllTasksPage> implements View {
  AllTasksModel _allTasksModel;
  AllTasksPresenter _allTasksPresenter;

  @override
  void initState() {
    _allTasksModel = AllTasksModel();
    _allTasksPresenter = AllTasksPresenter(this, context, _allTasksModel);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    context.watch<TasksStore>();
    _allTasksPresenter.refreshTasks();

    return Scaffold(
      appBar: AppBar(
        title: Text("AllTasksPage"),
      ),
      body: _getBodyWidget(),
    );
  }

  @override
  void showMessage(String message) {
    CommonMethods.showToast(message);
  }

  @override
  void updateView() {
    if (mounted) setState(() {});
  }

  Widget _getBodyWidget() {
    if (_allTasksModel.fetchingAllTasksLoadingState.isLoading) {
      return Center(
        child: LoadingWidget(),
      );
    } else {
      if (_allTasksModel.allTasks.isEmpty) {
        return Center(
          child: Text("There are no tasks"),
        );
      } else {
        return ListView.builder(
            itemCount: _allTasksModel.allTasks.length,
            itemBuilder: (context, itemIndex) {
              Task task = _allTasksModel.allTasks[itemIndex];
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    title: Text("${task.content}"),
                  ),
                  Divider(
                    height: 1,
                  ),
                ],
              );
            });
      }
    }
  }
}
