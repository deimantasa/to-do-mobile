abstract class Presenter{
	void refreshTasks();
}

abstract class View{
	void updateView();
	void showMessage(String message);
}