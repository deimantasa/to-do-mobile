// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Task _$TaskFromJson(Map<String, dynamic> json) {
  return Task(
    json['content'] as String,
    json['objectId'] as String,
    json['updated'] as int,
    json['created'] as int,
    json['isArchived'] as bool,
  );
}

Map<String, dynamic> _$TaskToJson(Task instance) => <String, dynamic>{
      'content': instance.content,
      'objectId': instance.objectId,
      'updated': instance.updated,
      'created': instance.created,
      'isArchived': instance.isArchived,
    };
