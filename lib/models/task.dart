import 'package:json_annotation/json_annotation.dart';
import 'package:to_do_list/models/user.dart';

part 'task.g.dart';

@JsonSerializable(explicitToJson: true)
class Task {
  String content;
  String objectId;
  int updated;
  int created;
  bool isArchived;
  @JsonKey(ignore: true)
  bool isSelected;

  Task(this.content, this.objectId, this.updated, this.created, this.isArchived) {
    this.isSelected = false;
  }

  Task.add(this.content, String userId){
    this.isSelected=false;
  }

  factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);

  Map<String, dynamic> toJson() => _$TaskToJson(this);
}
