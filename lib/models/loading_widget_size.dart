import 'package:flutter/cupertino.dart';

class LoadingWidgetSize {
  double radius;
  double strokeWidth;

  LoadingWidgetSize.custom({@required this.radius, @required this.strokeWidth});

  LoadingWidgetSize.small() {
    radius = 9;
    strokeWidth = 2;
  }

  LoadingWidgetSize.medium() {
    radius = 18;
    strokeWidth = 4;
  }
}
