import 'package:json_annotation/json_annotation.dart';
import 'package:to_do_list/models/task.dart';

part 'user.g.dart';

@JsonSerializable(explicitToJson: true)
class User {
  String name;
  String objectId;
  List<Task> tasks;


  User(this.name, this.objectId, this.tasks);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
