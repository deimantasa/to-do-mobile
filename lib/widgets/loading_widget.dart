import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:to_do_list/models/loading_widget_size.dart';

class LoadingWidget extends StatelessWidget {
  final LoadingWidgetSize loadingWidgetSize;

  const LoadingWidget({Key key, this.loadingWidgetSize}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (loadingWidgetSize != null) {
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: loadingWidgetSize.radius * 2,
            height: loadingWidgetSize.radius * 2,
            child: CircularProgressIndicator(
              strokeWidth: loadingWidgetSize.strokeWidth,
            ),
          ),
        ),
      );
    } else {
      return CircularProgressIndicator();
    }
  }
}
