import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;
import 'package:to_do_list/network/api.dart';
import 'package:to_do_list/util/env.dart';

class PushNotificationManager {
  static const kTopicAll = "all";

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  void subscribeToTopic(String topic) {
    _firebaseMessaging.subscribeToTopic(topic);
  }

  void unsubscribeFromTopic(String topic) {
    _firebaseMessaging.unsubscribeFromTopic(topic);
  }

  Future<bool> sendPushNotification(String title, String body, String topic, String userId) async {
    Map<String, dynamic> pushMessageMap = {
      "notification": {"title": "$title", "body": "$body"},
      "priority": "high",
      "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK", "userId": "$userId"},
      "to": "/topics/$topic"
    };

    final response = await http.post(Api.pushNotificationUrl(), body: json.encode(pushMessageMap), headers: {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: "key=${Env.data.cloudMessagingKey}"
    });
    return response.statusCode == 200;
  }
}
